/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import domini.Comic;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author David i Ángel
 */
@Repository
public interface RepositoriComicsSpring extends CrudRepository<Comic, Long> {

    /**
     * Mètode per buscar en el repositori el còmic que coincideixi amb la id
     * passada per paràmetre.
     * @param id
     * @return objecte còmic
     */
    Comic findById(long id);

    /**
     * Mètode per buscar en el repositori els còmics que coincideixin amb l'any
     * passat per paràmetre.
     * @param año
     * @return llista de còmics
     */
    List<Comic> findByAño(int año);

    /**
     * Mètode per buscar en el repositori els còmics que coincideixin amb el 
     * títol passat per paràmetre.
     * @param nom
     * @return llista de còmics
     */
    List<Comic> findByNom(String nom);
}
