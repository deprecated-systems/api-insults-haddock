/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domini;

import java.io.Serializable;
import java.util.Arrays;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Classe còmic que ens serveix per fer tot el tractament de dades.
 * @author David i Ángel
 */
@Entity
@Table(name="comic")
public class Comic implements Serializable {
    
    private long id;
    private String nom;
    private String nomOriginal;
    private int año;
    private String editorial;
    private int numPag;
    private String[] insults;
    
    public Comic() {
    }

    public Comic(String nom) {
        this.nom = nom;
    }

    public Comic(String nom, String nomOriginal, int año, String editorial, int numPag, String[] insults) {
        this.nom = nom;
        this.nomOriginal = nomOriginal;
        this.año = año;
        this.editorial = editorial;
        this.numPag = numPag;
        this.insults = insults;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNomOriginal() {
        return nomOriginal;
    }

    public void setNomOriginal(String nomOriginal) {
        this.nomOriginal = nomOriginal;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public int getNumPag() {
        return numPag;
    }

    public void setNumPag(int numPag) {
        this.numPag = numPag;
    }

    public String[] getInsults() {
        return insults;
    }

    public void setInsults(String[] insults) {
        this.insults = insults;
    }

    @Override
    public String toString() {
        return "Comic{" + "id=" + id + ", nom=" + nom + ", nomOriginal=" + nomOriginal + ", año=" + año + ", editorial=" + editorial + ", numPag=" + numPag + ", insults=" + Arrays.toString(insults) + '}';
    }

    
}
