/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import dao.RepositoriComicsSpring;
import domini.Comic;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Mètode per carregar les dades a la base de dades.
 * @author David i Ángel
 */
@Component
public class CarregaDades {

    @Autowired
    private RepositoriComicsSpring repositoryComics;
    
    @PostConstruct
    private void initDatabase() {
        // Create
        // Comic(String nom, String nomOriginal, int año, String editorial, int numPag, String[] insults)
        String[] insults={"Miserable wipper-snapper", "Meddlesome cabin-boy", "Miserable", "Wretch"};
        repositoryComics.save(new Comic("The Crab with the Golden Claws", "Le Crabe aux pinces d'or", 1940, "Casterman", 58, insults));
        String[] insults2={"Pyromaniac", "Gangster", "Rat", "Dynamiter"};
        repositoryComics.save(new Comic("The Shooting Star", "L'Étoile mystérieuse", 1941, "Casterman", 62, insults2));
    }
}
