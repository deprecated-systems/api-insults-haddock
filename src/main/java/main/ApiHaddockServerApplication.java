package main;

import domini.Comic;
import java.nio.charset.Charset;
import java.util.Scanner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication(scanBasePackages = {"dao", "domini", "servei", "main", "controlador"})
@EnableJpaRepositories("dao")
@EntityScan("domini")
public class ApiHaddockServerApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ApiHaddockServerApplication.class, args);
    }

    /**
     *
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        Scanner teclat = new Scanner(System.in);

        String titol, titolOri, editorial, insults, id;
        int año, numPag;
        String[] insultsArray;
        int op = 0;
        Comic c;
        Comic[] comics;

        while (op != 5) {
            System.out.println("1) Inserir còmic");
            System.out.println("2) Modificar còmic");
            System.out.println("3) Eliminar còmic");
            System.out.println("4) Consultar");
            System.out.println("5) Sortir");
            System.out.print("Selecciona una opció: ");
            op = teclat.nextInt();

            switch (op) {
                // Insertar un còmic
                case 1:
                    System.out.print("Introdueix el títol del còmic: ");
                    titol = teclat.next();
                    System.out.print("Introdueix el títol original: ");
                    titolOri = teclat.next();
                    System.out.print("Introdueix l'any de publicació: ");
                    año = teclat.nextInt();
                    System.out.print("Introdueix l'editorial: ");
                    editorial = teclat.next();
                    System.out.print("Introdueix el número de pàgines: ");
                    numPag = teclat.nextInt();
                    System.out.println("Introdueix els insults de Haddock serparats per una coma: ");
                    insults = teclat.next();
                    insultsArray = insults.split(",");

                    c = new Comic(titol, titolOri, año, editorial, numPag, insultsArray);
                    HttpEntity<Comic> comicHttp = new HttpEntity<>(c, headers);
                    restTemplate.exchange("http://localhost:8080/comic", HttpMethod.POST, comicHttp, String.class);
                    break;
                // Modificar un còmic
                case 2:
                    System.out.print("Introdueix el id del còmic que vols modificar: ");
                    id = teclat.next();

                    System.out.print("Introdueix el títol del còmic: ");
                    titol = teclat.next();
                    System.out.print("Introdueix el títol original: ");
                    titolOri = teclat.next();
                    System.out.print("Introdueix l'any de publicació: ");
                    año = teclat.nextInt();
                    System.out.print("Introdueix l'editorial: ");
                    editorial = teclat.next();
                    System.out.print("Introdueix el número de pàgines: ");
                    numPag = teclat.nextInt();
                    System.out.println("Introdueix els insults de Haddock serparats per una coma: ");
                    insults = teclat.next();
                    insultsArray = insults.split(",");

                    c = restTemplate.getForObject("http://localhost:8080/comic/{id}", Comic.class, id);
                    c.setNom(titol);
                    c.setNomOriginal(titolOri);
                    c.setAño(año);
                    c.setEditorial(editorial);
                    c.setNumPag(numPag);
                    c.setInsults(insultsArray);
                    restTemplate.put("http://localhost:8080/comic/{id}", c, id);

                    break;
                // Eliminar un còmic
                case 3:
                    System.out.print("Introdueix el id del còmic que vols elimninar: ");
                    id = teclat.next();

                    restTemplate.delete("http://localhost:8080/comic/{id}", id);

                    break;
                // Consultes
                case 4:
                    System.out.println("1) Llistar còmics");
                    System.out.println("2) Obtenir còmics per títol");
                    System.out.println("3) Obtenir còmics per any");
                    System.out.print("Selecciona una opció: ");
                    int op2 = teclat.nextInt();

                    switch (op2) {
                        // Tots
                        case 1:
                            comics = restTemplate.getForObject("http://localhost:8080/comics", Comic[].class);
                            for (Comic co : comics) {
                                System.out.println("\033[31m" + co);
                            }
                            break;
                        // Per títol
                        case 2:
                            System.out.print("Introdueix el títol: ");
                            String nom = teclat.next();
                            comics = restTemplate.getForObject("http://localhost:8080/comics/noms/{nom}", Comic[].class, nom);
                            for (Comic co : comics) {
                                System.out.println("\033[36m" + co);
                            }
                            break;
                        // Per any
                        case 3:
                            System.out.print("Introdueix l'any: ");
                            String any = teclat.next();
                            comics = restTemplate.getForObject("http://localhost:8080/comics/anys{año}", Comic[].class, any);
                            for (Comic co : comics) {
                                System.out.println("\033[36m" + co);
                            }
                            break;
                    }

                    break;
            }
        }
    }

}
