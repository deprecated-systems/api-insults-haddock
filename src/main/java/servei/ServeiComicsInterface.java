/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servei;

import domini.Comic;
import java.util.List;

/**
 *
 * @author David i Ángel
 */
public interface ServeiComicsInterface {

    /**
     * Mètode get per retornar la llista de còmics sencera.
     * 
     * @return la llista de còmics
     */
    public List<Comic> getComics();

    /**
     * Mètode per afegir un còmic passat per paràmetre.
     * 
     * @param c
     */
    public void addComic(Comic c);

    /**
     * Mètode que retorna l'objecte còmic que coincideix amb la id passada per
     * paràmetre.
     * 
     * @param id
     * @return el còmic que coincideix amb la id passada per paràmetre
     */
    public Comic getComic(long id);

    /**
     * Mètode per actualizar el còmic mitjançant un objecte de Comic i una id.
     * 
     * @param id
     * @param comic
     */
    public void updateComic(long id, Comic comic);

    /**
     * Mètode que elimina un objecte còmic.
     * 
     * @param id
     */
    public void removeComic(long id);

    /**
     * Mètode que retorna una llista de còmics amb l'any passat per paràmetre.
     * @param año
     * @return llista de còmics amb l'any passat per paràmetre
     */
    public List<Comic> getComicsAño(int año);

    /**
     * Mètode que retorna una llista de còmics amb el títol del còmic passat per
     * paràmetre.
     * 
     * @param nom
     * @return llista de còmics amb el nom/títol passat per paràmetre
     */
    public List<Comic> getComicsNom(String nom);
}
