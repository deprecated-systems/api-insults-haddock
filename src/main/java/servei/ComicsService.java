/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servei;

import dao.RepositoriComicsSpring;
import domini.Comic;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author David i Ángel
 */
@Service
public class ComicsService implements ServeiComicsInterface {

    @Autowired
    private RepositoriComicsSpring comics;

    /**
     * Mètode get per retornar la llista de còmics sencera.
     * 
     * @return la llista de còmics
     */
    @Override
    public List<Comic> getComics() {
        return (List<Comic>) comics.findAll();
    }
    
    /**
     * Mètode per afegir un còmic passat per paràmetre.
     * 
     * @param c
     */
    @Override
    public void addComic(Comic c) {
        comics.save(c);
    }

    /**
     * Mètode que retorna l'objecte còmic que coincideix amb la id passada per
     * paràmetre.
     * 
     * @param id
     * @return el còmic que coincideix amb la id passada per paràmetre
     */
    @Override
    public Comic getComic(long id) {
        return comics.findById(id);
    }

    /**
     * Mètode per actualizar el còmic mitjançant un objecte de Comic i una id.
     * 
     * @param id
     * @param comic
     */
    @Override
    public void updateComic(long id, Comic comic) {
        Comic c = comics.findById(id);
        c.setNom(comic.getNom());
        c.setNomOriginal(comic.getNomOriginal());
        c.setAño(comic.getAño());
        c.setEditorial(comic.getEditorial());
        c.setNumPag(comic.getNumPag());
        c.setInsults(comic.getInsults());
        comics.save(c);
    }
    
    /**
     * Mètode que elimina un objecte còmic.
     * 
     * @param id
     */
    @Override
    public void removeComic(long id) {
        comics.delete(comics.findById(id));
    }
    
    /**
     * Mètode que retorna una llista de còmics amb l'any passat per paràmetre.
     * @param año
     * @return llista de còmics amb l'any passat per paràmetre
     */
    @Override
    public List<Comic> getComicsAño(int año) {
        return comics.findByAño(año);
    }
    
    /**
     * Mètode que retorna una llista de còmics amb el títol del còmic passat per
     * paràmetre.
     * 
     * @param nom
     * @return llista de còmics amb el nom/títol passat per paràmetre
     */
    @Override
    public List<Comic> getComicsNom(String nom) {
        return comics.findByNom(nom);
    }
}
