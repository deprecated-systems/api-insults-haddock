/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import domini.Comic;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import servei.ComicsService;

/**
 *
 * @author David i Ángel
 */
@RestController
public class ComicsControlador {
    
    @Autowired
    private ComicsService serveiComics;

    /**
     * Mètode que retorna tots els codis emmagatzemats a la bd en format JSON.
     * @return llista de còmics
     */
    @RequestMapping(value = "/comics", method = RequestMethod.GET)
    public List<Comic> obtenirComics() {
        return serveiComics.getComics();
    }
    
    /**
     * Mètode per obtenir un còmic mitjançant la id.
     * @param id
     * @return còmic en format JSON
     */
    @RequestMapping(value = "/comic/{id}", method = RequestMethod.GET)
    public Comic obtenirComic(@PathVariable("id") long id) {
        return serveiComics.getComic(id);
    }
    
    /**
     * Mètode per crear un còmic.
     * @param comic
     * @return resposta del servidor
     */
    @RequestMapping(value = "/comic", method = RequestMethod.POST)
    public ResponseEntity<Boolean> afegirComic(@RequestBody Comic comic) {
        serveiComics.addComic(comic);
        return new ResponseEntity<>(true, HttpStatus.CREATED);
    }
    
    /**
     * Mètode per modificar un còmic.
     * @param id
     * @param comic
     * @return resposta del servidor
     */
    @RequestMapping(value = "/comic/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Boolean> modificarComic(@PathVariable("id") long id, @RequestBody Comic comic) {
        serveiComics.updateComic(id, comic);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
    
    /**
     * Mètode per eliminar un còmic.
     * @param id
     * @return resposta del servidor
     */
    @RequestMapping(value = "/comic/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> eliminarComic(@PathVariable("id") long id) {
        serveiComics.removeComic(id);
        return new ResponseEntity<>(true, HttpStatus.NO_CONTENT);
    }
    
    /**
     * Mètode per buscar per nom.
     * @param año
     * @return llista de còmics en format JSON
     */
    @RequestMapping(value = "/comics/anys/{año}", method = RequestMethod.GET)
    public List<Comic> obtenirComics(@PathVariable("año") int año) {
        return serveiComics.getComicsAño(año);
    }
    
    /**
     * Mètode per buscar per nom.
     * @param nom
     * @return llista de còmics en format JSON
     */
    @RequestMapping(value = "/comics/noms/{nom}", method = RequestMethod.GET)
    public List<Comic> obtenirComics(@PathVariable("nom") String nom) {
        return serveiComics.getComicsNom(nom);
    }
}